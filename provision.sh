#!/bin/bash

HOMEDIR=/home/vagrant

# Updates
sudo apt-get -y update
sudo apt-get -y install tmux
sudo apt-get -y install vim
sudo apt-get -y install gdb gdb-multiarch
sudo apt-get -y install ipython
sudo apt-get -y install binutils
sudo apt-get -y install binwalk
sudo apt-get -y install build-essential
sudo apt-get -y install git
sudo apt-get -y install nasm
sudo apt-get -y install netcat-traditional
sudo apt-get -y install nmap
sudo apt-get -y install openssl
sudo apt-get -y install p7zip
sudo apt-get -y install scapy
sudo apt-get -y install socat
sudo apt-get -y install python3-pip
sudo apt-get -y install python-setuptools
sudo apt-get -y install python-dev
sudo apt-get -y install python2.7
sudo apt-get -y install python2.7-dev
sudo apt-get -y install python-pip
sudo apt-get -y install software-properties-common
sudo apt-get -y install libcapstone3
sudo apt-get -y install libffi-dev
sudo apt-get -y install virtualenvwrapper
sudo apt-get -y install outguess
sudo apt-get -y install steghide
sudo apt-get -y install fcrackzip
sudo apt-get -y install pdfcrack
sudo apt-get -y install rarcrack
sudo apt-get -y install john
sudo apt-get -y install mc
sudo apt-get -y install tshark
sudo apt-get -y install imagemagick
sudo apt-get -y install pngtools
sudo apt-get  -y install python3-dev python3-setuptools
suod apt-get -y install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
sudo apt-get -y install samdump2 bkhive
sudo apt-get -y install libimage-exiftool-perl
sudo apt-get -y install libimage-info-perl
sudo apt-get -y install upx-ucl 
sudo dpkg --add-architecture i386
sudo apt-get -y install libc6:i386 libncurses5:i386 libstdc++6:i386
ulimit -c unlimited

# Checksec
cd $HOMEDIR
wget http://www.trapkit.de/tools/checksec.sh

# capstone
sudo pip install capstone --upgrade
# sudo apt-get install libcapstone-dev

# pwntools
sudo apt-add-repository -y ppa:pwntools/binutils
sudo apt-get update
sudo sudo apt-get install -qy binutils-{aarch64,alpha,arm,avr,cris,hppa,i386,ia64,m68k,msp430,powerpc{,64},sparc{,64},vax,xscale}-linux-gnu
sudo pip install pwntools
#  pip install git+https://github.com/Gallopsled/pwntools#egg=pwntools

# Angr
#sudo pip install angr --upgrade

# ROPGadget
sudo pip install ropgadget

#unicorn
#git clone https://github.com/unicorn-engine/unicorn.git
#sudo apt-get install libglib2.0-dev
#cd unicorn
#./make.sh
#./make.sh install

# peda
#git clone https://github.com/longld/peda.git ~/peda
#echo "source ~/peda/peda.py" >> ~/.gdbinit

# Install Qira
#cd $HOMEDIR
#git clone https://github.com/BinaryAnalysisPlatform/qira.git
#cd qira
#./install.sh
#./fetchlibs.sh
#./tracers/pin_build.sh


# Install Binjitsu
pip install --upgrade git+https://github.com/binjitsu/binjitsu.git

cd $HOMEDIR
git clone https://github.com/radare/radare2
cd radare2
./sys/install.sh


cd /home/vagrant/ctf-tools/

git clone https://github.com/Owlz/StegoDone
git clone https://github.com/ius/rsatool
git clone https://github.com/hellman/xortool
git clone https://github.com/magnumripper/JohnTheRipper
git clone https://github.com/JonathanSalwan/ROPgadget
git clone https://github.com/BinaryAnalysisPlatform/qira
git clone https://github.com/androguard/androguard
git clone https://github.com/Storyyeller/Krakatau
git clone https://github.com/sqlmapproject/sqlmap

# Install pillow
sudo apt-get -y install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
sudo pip3 install numpy
sudo pip3 install pillow

# Personal config
sudo sudo apt-get -y install stow
cd $HOMEDIR
git clone --recursive https://github.com/phm3/config
cd config
./install.sh




